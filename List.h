#pragma once
template <typename T>
class List
{
	struct ListNode
	{
		ListNode()
		{
		}
		T mData;
		ListNode *mPrev;
		ListNode *mNext;
	};
	ListNode *mHead;
	ListNode *mTail;
	int mSize = 0;
	void Init()
	{
		mHead = new ListNode;
		mTail = new ListNode;
		mHead->mNext = mTail;
		mHead->mPrev = nullptr;
		mTail->mPrev = mHead;
		mTail->mNext = nullptr;
	}
public:
	List()
	{
		Init();
	}
	List(const List & tOther)
	{
		auto j = tOther.Size();

		Init();

		for (int i = 0; i < j; i++) {
			PushBack(tOther.At(i));
		}
	}
	List & operator = (const List & tRHS)
	{
		ListNode *temp = new ListNode;
		for (int i = 0; i < tRHS.Size(); i++) {
			temp->mData = tRHS.At(i);
			mHead->mNext = temp;
			mTail->mPrev = temp;
		}
		delete temp;
	}
	~List()
	{
		Clear();
		delete mHead;
		delete mTail;
	}
	void PushFront(const T &tWhat)
	{
		ListNode *temp = new ListNode;
		temp->mData = tWhat;
		temp->mHead = mHead;
		mHead->mPrev = temp;
		temp->mPrev = nullptr;
		mHead = temp;
	}
	void PopFront()
	{
		ListNode *temp = mHead;

		mHead = mHead->mNext;
		mHead->mPrev = nullptr;

		delete temp;
		mSize--;
	}
	T& Front()
	{
		return mHead->mNext->mData;
	}
	void PushBack(const T &tWhat)
	{
		ListNode *temp = new ListNode;
		ListNode *prev = mTail->mPrev;

		prev->mNext = temp;

		temp->mData = tWhat;
		temp->mNext = mTail;
		temp->mPrev = prev;

		mTail->mPrev = temp;

		++mSize;
	}
	void PopBack()
	{
		ListNode *temp = mTail;

		mTail = mTail->mPrev;
		mTail->mNext = nullptr;

		delete temp;
	}
	T& Back()
	{
		return mTail->mPrev->mData;
	}
	int Size() const
	{
		return mSize;
	}
	void Clear()
	{
		ListNode* current = mHead;

		while (current != mTail) {
			ListNode* tmp = current;
			current = current->mNext;
			delete tmp;
		}
	}
	T At(int tWhere) const
	{
		ListNode *CurrentPos = mHead->mNext;

		for (int i = 0; i < Size(); i++) {
			if (i == tWhere) {
				return CurrentPos->mData;
			}
			CurrentPos = CurrentPos->mNext;
		}

		return 0;
	}
	///////////////////////////////////////////////////////////////////
	// Iterators
	class Iterator
	{
		ListNode *mCurrent;
	public:
		Iterator(ListNode *tStart)
		{
			mCurrent = tStart;
		}
		T& GetData()
		{
			return mCurrent->mData;
		}
		void Next()// As in "Move to the next item please".
		{
			mCurrent = mCurrent->mNext;
		}
		bool IsEqual(const Iterator &rhs)
		{
			if (rhs.mCurrent == this->mCurrent) {
				return true;
			}
			else return false;
		}
	};
	Iterator Insert(Iterator tWhere, const T &tWhat)
	{
		ListNode *PreviousPos = new ListNode;
		ListNode *CurrentPos = new ListNode;
		ListNode *tempPos = new ListNode;
		CurrentPos = mHead;
		for (int i= 1; i < tWhere; i++) {
			PreviousPos = CurrentPos;
			CurrentPos = CurrentPos->mNext;
		}
		tempPos->mData = tWhat;
		PreviousPos->mNext = tempPos;
		tempPos->mNext = CurrentPos;
		delete PreviousPos;
		delete CurrentPos;
		delete tempPos;
	}
	Iterator Erase(Iterator tWhat)
	{
		ListNode *CurrentPos = new ListNode;
		ListNode *PreviousPos = new ListNode;
		CurrentPos = mHead;
		for (int i = 1; i < tWhat; i++) {
			PreviousPos = CurrentPos;
			CurrentPos = CurrentPos->mNext;
		}
		PreviousPos->mNext = CurrentPos->mNext;
		delete CurrentPos;
		delete PreviousPos;
	}
	Iterator Begin()
	{
		// First good data
		Iterator tReturn(mHead->mNext);
		return tReturn;
	}
	Iterator End()
	{
		// First Bad data
		Iterator tReturn(mTail);
		return tReturn;
	}
};